# Fork List for Bitbucket Server
This plugin shows all the forks for a repository and all
of the forks for a project.

Please post any issues in the issues section.

## Screenshots
![Screen Shot 2014-12-13 at 3.00.16 PM.png](https://bitbucket.org/repo/rXMRae/images/2461458613-Screen%20Shot%202014-12-13%20at%203.00.16%20PM.png)

![Screen Shot 2014-12-13 at 2.59.25 PM.png](https://bitbucket.org/repo/rXMRae/images/740110611-Screen%20Shot%202014-12-13%20at%202.59.25%20PM.png)

![Screen Shot 2014-12-13 at 4.27.43 PM.png](https://bitbucket.org/repo/rXMRae/images/180822517-Screen%20Shot%202014-12-13%20at%204.27.43%20PM.png)
